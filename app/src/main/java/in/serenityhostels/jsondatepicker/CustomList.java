package in.serenityhostels.jsondatepicker;

import android.app.Activity;
import android.graphics.Color;
import android.support.v4.app.NotificationCompatSideChannelService;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;

public class CustomList extends ArrayAdapter<String>{

    private final Activity context;
    private final String[] l_date;
    private final String[] l_status;
    public CustomList(Activity context,
                      String[] l_date, String[] l_status) {
        super(context, R.layout.layout_single, l_date);
        this.context = context;
        this.l_date = l_date;
        this.l_status=l_status;

    }
    @Override
    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.layout_single, null, true);
        TableRow tableRow = (TableRow)rowView.findViewById(R.id.tab_row);
        tableRow.setBackgroundColor(Color.parseColor("#C62828"));
        TextView date = (TextView) rowView.findViewById(R.id.date);
        TextView status = (TextView) rowView.findViewById(R.id.status);
        if(l_status[position].equals("ABSENT"))
        {
            tableRow.setBackgroundColor(Color.parseColor("#D50000"));
        }
        else  if(l_status[position].equals("PRESENT"))
        {
            tableRow.setBackgroundColor(Color.parseColor("#00B0FF"));
        }
        else  if(l_status[position].equals("NIGHT OUT"))
        {
            tableRow.setBackgroundColor(Color.parseColor("#000000"));
        }
        else  if(l_status[position].equals("LEAVE"))
        {
            tableRow.setBackgroundColor(Color.parseColor("#D50000"));
        }
        date.setText(l_date[position]);
        status.setText(l_status[position]);
        return rowView;
    }
}