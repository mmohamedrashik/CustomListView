package in.serenityhostels.jsondatepicker;

import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
Spinner spinner,spinner2;
Button button;
ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final String[] months = {"ALL","JUNE","JULY","AUGUST","SEPTEMBER","NOVEMBER","DECEMBER","JANUARY","FEBRUARY","MARCH","APRIL","MAY"};
        String[] year = {"2015","2016","2017","2018"};
        final String[] date = {"2-01-2015","3-01-2015","4-01-2015","5-01-2015","6-01-2015","7-01-2015","8-01-2015","9-01-2015","10-01-2015"
                ,"11-01-2015","12-01-2015","13-01-2015"};
        final String[] status = {"PRESENT","ABSENT","PRESENT","NIGHT OUT","PRESENT","LEAVE","LEAVE","PRESENT","PRESENT"
                ,"PRESENT","PRESENT","PRESENT"};
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        spinner = (Spinner)findViewById(R.id.month);
        spinner2 = (Spinner)findViewById(R.id.year);
        button = (Button)findViewById(R.id.search);
        listView = (ListView)findViewById(R.id.search_result);

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, months);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_dropdown_item,year);
        spinner.setAdapter(adapter);
        spinner2.setAdapter(adapter1);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Search(spinner.getSelectedItem().toString(), spinner2.getSelectedItem().toString());
                CustomList adapter = new CustomList(MainActivity.this, date, status);
                listView.setAdapter(adapter);
            }
        });
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),"DATE :"+date[position]+" STATUS :"+status[position],Toast.LENGTH_LONG).show();
                FragmentManager fragmentManager = getSupportFragmentManager();
                Bundle bundle = new Bundle();
                bundle.putString("date",date[position]);
                bundle.putString("status",status[position]);
                ADetails aDetails = new ADetails();
                aDetails.setArguments(bundle);
                aDetails.show(fragmentManager,"asd");
            }
        });


    }
    public void Search(String month,String year)
    {
        Toast.makeText(getApplicationContext(),""+month+"--"+year,Toast.LENGTH_LONG).show();
    }
}
