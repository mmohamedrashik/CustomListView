package in.serenityhostels.jsondatepicker;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Mohammed Razik on 12/29/2015.
 */
public class ADetails extends DialogFragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.attendance_details, container, false);
        Bundle bundle = getArguments();
        TextView dates=(TextView)view.findViewById(R.id.d_date);
        TextView status =(TextView)view.findViewById(R.id.d_status);
        dates.setText(bundle.getString("date").toString());
        status.setText(bundle.getString("status").toString());
        return view;
    }
}
